import util.OpenCV_33_FaceSwap;

/**
 * @ClassName: TestFaceMerge
 * @description: 示例代码
 * @author: 小帅丶
 * @create: 2019-05-18
 **/
public class TestFaceMerge {
    public static void main(String[] args) {
        String path1 = "C:\\Users\\liww\\Pictures\\facedetect\\5.png";//原图1
        String path2 = "C:\\Users\\liww\\Pictures\\facedetect\\1.jpeg";//原图2
        String filename = "C:\\Users\\liww\\Pictures\\facedetect\\output.jpg";//融合图
        OpenCV_33_FaceSwap.faceMerge(path1,path2,filename);
    }

}
